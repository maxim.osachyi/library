import { Component, OnInit } from '@angular/core';
import { BOOKS } from '../../../../config';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss'],
})
export class AddBookComponent implements OnInit {
  storageKey = 'books';
  navigateToList = BOOKS;
  booksFrom: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.booksFrom = this.fb.group({
      id: Math.random().toString(36).substr(2, 9),
      name: ['', Validators.required],
      author: ['', Validators.required],
      image: [null, Validators.required],
      is_default: false,
    });
  }
}
