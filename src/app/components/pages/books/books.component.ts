import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ADD_BOOK, DEFAULT_BOOKS } from '../../../config';
import { Router } from '@angular/router';
import { HelperService } from '../../../services/helper.service';
import { TableColumn } from '../../partials/table/table-column.interface';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
})
export class BooksComponent implements OnInit, AfterViewInit {
  title = 'Books List';
  addBookBtn = 'Add Book';
  booksColumns: TableColumn<any>[] = [
    { label: 'id' },
    { label: 'image' },
    { label: 'name' },
    { label: 'author' },
    { label: 'amount' },
    { label: 'controls' },
  ];

  constructor(private router: Router, private hs: HelperService) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    if (localStorage.getItem('books') === null) {
      this.setDefaultBooks();
    }
  }

  setDefaultBooks() {
    localStorage.setItem('books', JSON.stringify(DEFAULT_BOOKS));
  }

  removeBook(bookId: number) {
    const index: any = this.hs.booksArray.findIndex((item: any) => {
      return item.id === bookId;
    });

    this.hs.booksArray.splice(index, 1);
    localStorage.setItem('books', JSON.stringify(this.hs.booksArray));
  }

  addBook() {
    this.router.navigate([ADD_BOOK]);
  }

  editBook(bookId: number) {
    this.router.navigate([ADD_BOOK], { queryParams: { bookId } });
  }

  get defaultBooks() {
    if (localStorage.getItem('books')) {
      return (this.hs.booksArray = JSON.parse(
        localStorage.getItem('books') || ''
      ));
    }
  }
}
