import { AfterViewInit, Component } from '@angular/core';
import { Router } from '@angular/router';
import { HelperService } from '../../../services/helper.service';
import { ADD_USER, DEFAULT_USERS } from '../../../config';
import { TableColumn } from '../../partials/table/table-column.interface';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements AfterViewInit {
  title = 'Users List';
  addUserBtn = 'Add User';
  usersColumns: TableColumn<any>[] = [
    { label: 'id' },
    { label: 'image' },
    { label: 'name' },
    { label: 'email' },
    { label: 'controls' },
  ];
  constructor(private router: Router, private hs: HelperService) {}

  ngAfterViewInit(): void {
    if (localStorage.getItem('users') === null) {
      this.setDefaultUsers();
    }
  }

  setDefaultUsers() {
    localStorage.setItem('users', JSON.stringify(DEFAULT_USERS));
  }

  removeUser(userId: number) {
    const index: any = this.hs.usersArray.findIndex((item: any) => {
      return item.id === userId;
    });

    this.hs.usersArray.splice(index, 1);
    localStorage.setItem('users', JSON.stringify(this.hs.usersArray));
  }

  addUsers() {
    this.router.navigate([ADD_USER]);
  }

  editUser(userId: number) {
    this.router.navigate([ADD_USER], { queryParams: { userId } });
  }

  get defaultUsers() {
    if (localStorage.getItem('users')) {
      return (this.hs.usersArray = JSON.parse(
        localStorage.getItem('users') || ''
      ));
    }
  }
}
