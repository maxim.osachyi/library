import { Component, OnInit } from '@angular/core';
import { USERS } from '../../../../config';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  storageKey = 'users';
  navigateToList = USERS;
  usersFrom: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.usersFrom = this.fb.group({
      id: Math.random().toString(36).substr(2, 9),
      name: ['', Validators.required],
      email: ['', Validators.required],
      image: [null, Validators.required],
      is_default: false,
    });
  }
}
