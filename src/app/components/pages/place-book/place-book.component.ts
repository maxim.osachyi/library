import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  toArray,
} from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelperService } from '../../../services/helper.service';

@Component({
  selector: 'app-place-book',
  templateUrl: './place-book.component.html',
  styleUrls: ['./place-book.component.scss'],
})
export class PlaceBookComponent implements OnInit {
  @ViewChild('emailInput', { static: true }) emailInput: ElementRef;
  @ViewChild('bookInput', { static: true }) bookInput: ElementRef;
  takeBookForm: FormGroup;
  usersArray: any;
  booksArray: any;
  user: any;
  book: any;
  constructor(private fb: FormBuilder, private hs: HelperService) {}

  ngOnInit(): void {
    this.createFrom();
    this.getUsers();
    this.getBooks();
  }

  createFrom() {
    this.takeBookForm = this.fb.group({
      userEmail: [null, Validators.required],
      bookName: [null, Validators.required],
    });
  }

  onUserSelect(selectedUser: any) {
    if (selectedUser) {
      this.getUser(selectedUser);
      this.takeBookForm.get('userEmail')!.patchValue(selectedUser);
    }
  }

  getUsers() {
    this.hs.getUsers()
      .pipe(
        map(({ email }) => email),
        toArray()
      )
      .subscribe((val) => {
        this.usersArray = val;
      });

    // if (localStorage.getItem('users') !== null) {
    //   this.hs.getUsers();
    //   this.usersArray = this.hs.usersArray.map((item: any) => {
    //     return item.email;
    //   });
    // }
  }

  getUser(searchString: any) {
    this.user = this.usersArray.find((item: any) => {
      return item.email === searchString;
    });
  }

  onBookSelect(selectedBook: any) {
    if (selectedBook) {
      this.getBook(selectedBook);
      this.takeBookForm.get('bookName')!.patchValue(selectedBook);
    }
  }

  getBooks() {
    if (localStorage.getItem('books') !== null) {
      this.hs.getBooks();
      const filterBooks = this.hs.booksArray.filter((book: any) => {
        return book.amount !== 0;
      });
      this.booksArray = filterBooks.map((item: any) => {
        return item.name;
      });
    }
  }

  getBook(searchString: any) {
    this.book = this.booksArray.find((item: any) => {
      return item.name === searchString;
    });
  }

  findEmail() {
    fromEvent(this.emailInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => {
          return event.target.value;
        }),
        filter((res) => res.length > 2 || res.length === 0),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(() => {});
  }

  onSubmit() {
    if (this.takeBookForm.valid) {
    }
  }

  public onInputSymbol(term: string, item: any) {
    const lowerCaseTerm = term.toLowerCase();
    const itemName = item.substring(0, term.length).toLowerCase();

    return itemName.indexOf(lowerCaseTerm) > -1 ? item : '';
  }
}
