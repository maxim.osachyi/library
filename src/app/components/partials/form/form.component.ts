import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelperService } from '../../../services/helper.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

const MAX_FILE_SIZE_IN_BYTES = 6000000;

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit, OnDestroy {
  editItem: any;
  @Input() editFormFields: any;
  @Input() storageKey: string;
  @Input() navigateToList: string;
  @Input() form: FormGroup;
  formError = false;
  fileName: any;
  fileReader: any;
  formData: any;
  subscription: Subscription = new Subscription();
  constructor(
    private fb: FormBuilder,
    public hs: HelperService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.queryParamsSubscription();
    if (typeof window !== 'undefined') {
      this.fileReader = new FileReader();
      this.fileReader.onload = (event: any) => {
        this.form.get('image')?.patchValue(event.target.result);
      };
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  createForm() {
    if (this.editItem) {
      if (this.form.get('author')) {
        this.form.patchValue({
          id: this.editItem.id,
          image: this.editItem.image,
          name: this.editItem.name,
          author: this.editItem.author,
          is_default: this.editItem.is_default,
        });
      } else if (this.form.get('email')) {
        this.form.patchValue({
          id: this.editItem.id,
          image: this.editItem.image,
          name: this.editItem.name,
          email: this.editItem.email,
          is_default: this.editItem.is_default,
        });
      }
    }
  }

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.fileName = file.name;
      if (
        !this.fileValidate(file.type) ||
        event.target.files[0].size > MAX_FILE_SIZE_IN_BYTES
      ) {
        alert('File format no valid');
        return;
      }

      if (this.fileReader) {
        this.fileReader.readAsDataURL(file);
      }
    }
  }

  fileValidate(type: string) {
    const validFilesTypes = [
      'application/pdf',
      'image/jpg',
      'image/jpeg',
      'image/bmp',
      'image/gif',
      'image/png',
    ];
    let validType = false;

    for (const validator of validFilesTypes) {
      if (validator === type) {
        validType = true;
        break;
      }
    }

    return validType;
  }

  onSubmit() {
    if (this.form.valid) {
      this.getFormData();
      const formData = JSON.parse(localStorage.getItem(this.storageKey) || '');
      this.hs.navToCurrentPage(this.navigateToList);
      if (this.editItem) {
        const currentBookIndex = formData.findIndex((item: any) => {
          return item.id === this.editItem.id;
        });
        formData.splice(currentBookIndex, 1, this.formData);
        console.log(currentBookIndex);
      } else {
        formData.push(this.formData);
      }

      localStorage.setItem(this.storageKey, JSON.stringify(formData));
    } else {
      if (this.form.controls.image.status === 'INVALID') {
        this.formError = true;
      }
    }
  }

  getFormData() {
    if (this.form.get('author')) {
      this.formData = {
        id: this.form.value.id,
        image: this.form.value.image,
        name: this.form.value.name,
        author: this.form.value.author,
        is_default: this.form.value.is_default,
      };
    } else if (this.form.get('email')) {
      this.formData = {
        id: this.form.value.id,
        image: this.form.value.image,
        name: this.form.value.name,
        email: this.form.value.email,
        is_default: this.form.value.is_default,
      };
    }
  }

  queryParamsSubscription() {
    this.subscription = this.route.queryParams.subscribe((params) => {
      let itemId: string;
      if (params && params.bookId) {
        itemId = params.bookId;
      } else {
        itemId = params.userId;
      }

      const itemArray = JSON.parse(localStorage.getItem(this.storageKey) || '');
      this.editItem = itemArray.find((item: any) => {
        return item.id === itemId;
      });

      this.createForm();
    });
  }
}
