import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input() title: string;
  @Input() data: any;
  @Input() columns: any;
  @Input() addButtonText: string;
  @Output() _addItem: EventEmitter<any> = new EventEmitter();
  @Output() _editItem: EventEmitter<any> = new EventEmitter();
  @Output() _removeItem: EventEmitter<any> = new EventEmitter();
  constructor(public router: Router) {}

  ngOnInit(): void {}

  addItem() {
    this._addItem.emit();
  }

  editItem(itemId: any) {
    this._editItem.emit(itemId);
  }

  removeItem(itemId: any) {
    this._removeItem.emit(itemId);
  }

  get visibleColumns() {
    return this.columns.map((column: any) => column.label);
  }
}
