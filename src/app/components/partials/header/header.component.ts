import { Component, OnInit } from '@angular/core';
import { BOOKS, DASHBOARD, PLACE_BOOK, USERS } from '../../../config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  dashboard = DASHBOARD;
  placeBook = PLACE_BOOK;
  books = BOOKS;
  users = USERS;
  showNavBar = false;
  constructor() {}

  ngOnInit(): void {}

  toggleNavBar() {
    this.showNavBar = !this.showNavBar;
  }
}
