import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';
import { HeaderComponent } from './components/partials/header/header.component';
import { PlaceBookComponent } from './components/pages/place-book/place-book.component';
import { BooksComponent } from './components/pages/books/books.component';
import { UsersComponent } from './components/pages/users/users.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TableComponent } from './components/partials/table/table.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import {MatButtonModule} from "@angular/material/button";
import { AddBookComponent } from './components/pages/books/add-book/add-book.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import { AddUserComponent } from './components/pages/users/add-user/add-user.component';
import { FormComponent } from './components/partials/form/form.component';
import {NgSelectModule} from "@ng-select/ng-select";

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    PlaceBookComponent,
    BooksComponent,
    UsersComponent,
    TableComponent,
    AddBookComponent,
    AddUserComponent,
    FormComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatButtonModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        NgSelectModule,
    ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
