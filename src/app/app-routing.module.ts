import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  ADD_BOOK,
  ADD_USER,
  BOOKS,
  DASHBOARD,
  PLACE_BOOK,
  USERS,
} from './config';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';
import { PlaceBookComponent } from './components/pages/place-book/place-book.component';
import { BooksComponent } from './components/pages/books/books.component';
import { UsersComponent } from './components/pages/users/users.component';
import { AddBookComponent } from './components/pages/books/add-book/add-book.component';
import { AddUserComponent } from './components/pages/users/add-user/add-user.component';

const routes: Routes = [
  { path: '', redirectTo: BOOKS, pathMatch: 'full' },
  { path: DASHBOARD, component: DashboardComponent },
  { path: PLACE_BOOK, component: PlaceBookComponent },
  { path: BOOKS, component: BooksComponent },
  { path: ADD_BOOK, component: AddBookComponent },
  { path: USERS, component: UsersComponent },
  { path: ADD_USER, component: AddUserComponent },
  { path: '**', component: BooksComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
