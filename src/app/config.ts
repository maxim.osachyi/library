export const DASHBOARD = 'dashboard';
export const PLACE_BOOK = 'place-book';
export const BOOKS = 'books';
export const ADD_BOOK = 'books/add-book';
export const USERS = 'users';
export const ADD_USER = 'users/add-user';

export const DEFAULT_BOOKS = [
  {
    id: '1',
    image: '../assets/images/clean-code.jpg',
    name: 'Чистый код',
    author: 'Роберт Мартин',
    amount: 0,
    is_default: true
  },
  {
    id: '2',
    image: '../assets/images/javascript.jpg',
    name: 'JavaScript: Полное руководство',
    author: 'Дэвид Флэнаган',
    amount: 2,
    is_default: true
  },
  {
    id: '3',
    image: '../assets/images/html-css.jpg',
    name: 'HTML5 и CSS3 для чайников',
    author: 'Эд Титтел, Крис Минник',
    amount: 2,
    is_default: true
  },
  {
    id: '4',
    image: '../assets/images/java.jpg',
    name: 'Java: эффективное программирование',
    author: 'Джошуа Блох',
    amount: 2,
    is_default: true
  },
  {
    id: '5',
    image: '../assets/images/c.jpg',
    name: 'Язык программирования C',
    author: 'Стивен Прата',
    amount: 2,
    is_default: true
  }
];

export const DEFAULT_USERS = [
  {
    id: '1',
    image: '../assets/images/clean-code.jpg',
    name: 'Константин',
    email: 'Kastet',
    is_default: true
  },
  {
    id: '2',
    image: '../assets/images/javascript.jpg',
    name: 'Иван',
    email: 'Johny',
    is_default: true
  },
  {
    id: '3',
    image: '../assets/images/html-css.jpg',
    name: 'Александр',
    email: 'Sash',
    is_default: true
  },
  {
    id: '4',
    image: '../assets/images/java.jpg',
    name: 'Максим',
    email: 'Max',
    is_default: true
  },
  {
    id: '5',
    image: '../assets/images/c.jpg',
    name: 'Кирилл',
    email: 'Kill',
    is_default: true
  }
];
