import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {from, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HelperService {
  booksArray: any = [];
  usersArray: any = [];
  constructor(private router: Router) {}

  navToCurrentPage(page: string) {
    this.router.navigate([page]);
  }

  getUsers(): Observable<any> {
    return from(JSON.parse(localStorage.getItem('users') || ''));
  }

  // getUsers() {
  //   this.usersArray = JSON.parse(localStorage.getItem('users') || '');
  // }

  getBooks() {
    this.booksArray = JSON.parse(localStorage.getItem('books') || '');
  }
}
